Angular 2 App Written in TypeScript w/o Typings
-----------------------------------------------
# What is this?

# Installation
Clone the repo and install it with npm. Obviously, you'll need node v5.1.0 installed globally. We recommend installing it with [nvm](https://github.com/creationix/nvm).
```
npm install
```
This will install all the dependencies for the app.

# Run
```
npm start
```

# Build
```
npm run build
```

# Stages

The app has been developed in stages. Each stage corresponds to a point in a progressive development approach:

basic-app - basic app
activities-service  - adds service to fetch activities from server
activities-repeater - adds displaying activities in basic repeater
list-component - adds component to display each activity
basic-routing - adds basic routing to app
tasks-routing - adds tasks service and routing for tasks

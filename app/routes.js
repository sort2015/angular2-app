import {Activities} from "./components/activities";
import {Tasks} from "./components/tasks";

export const ROUTES_CONFIG = [
  {path: '/', redirectTo: "/activities"},
  {path: '/activities', as: "Activities", component: Activities},
  {path: '/tasks', as: "Tasks", component: Tasks}
]

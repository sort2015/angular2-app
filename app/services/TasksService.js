import {Injectable} from "angular2/angular2";

@Injectable()
export class TasksService {
  tasks = [];

  getTasks() {
    return Promise.resolve(this.tasks);
  }

  addTask(task) {
    console.log("task", task);
    var current = this.tasks.find(tsk => {return tsk.id === task.id});
    if (!current) this.tasks.push(task);
  }

}

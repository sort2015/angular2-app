import {Injectable} from "angular2/angular2";
import {Http} from "angular2/http";
import moment from "moment";

@Injectable()
export class ActivitiesService {
  activities = [];

  constructor(http: Http) {
    console.log("constructor", constructor);
    this.http = http;
  }

  getActivities() {
    return new Promise((resolve, reject) => {
      if (this.activities.length > 0) resolve(this.activities);
      else {
        this.http.get("/api/activity").subscribe(res => {
          if (res.statusText === 'Ok') {
            let data = res.json();
            data.forEach(activity => {
              let date = moment(activity.timestamp);
              activity.date = date.format("MM/DD, hA");
              activity.timeago = date.fromNow();
            });
            this.activities = data;
            resolve(this.activities);
          } else {
            reject(res.statusText);
          }
        });
      }
    });
  }
}

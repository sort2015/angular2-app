import {ActivityTask} from "./activity-task";
import {Component, NgFor} from "angular2/angular2";
import {TasksService} from "../services/TasksService";

@Component({
  directives: [ActivityTask, NgFor],
  selector: 'tasks',
  template: `
  <div *ng-for='#task of tasks'>
    <activity-task [item]="task"></activity-task>
  </div>
  `
})

export class Tasks {
  tasks = [];

  constructor(public tasksService: TasksService) {
    tasksService.getTasks()
      .then(tasks => this.tasks = tasks);
  }


}

import {Component, CORE_DIRECTIVES, EventEmitter, Input, Output, ViewEncapsulation} from 'angular2/angular2';

let template = `
<div class="pure-g item">
  <div class="pure-u-sm-1-5">
    <img [src]="item.image" alt=""></img>
  </div>
  <div class="pure-u-sm-4-5">
    <div class="pure-g">
      <div class="pure-u-1">
        <div class="title">{{item.title}}</div>
      <div>
    </div>
    <div class="pure-g">
      <div class="pure-u-1">
        <div class="description">{{item.description}}</div>
      <div>
    </div>
    <div class="pure-g">
      <div class="pure-u-1">
        <div class="date">{{item.date}} / {{item.timeago}}</date>
      </div>
    </div>
    <div class="pure-g">
      <div class="pure-u-1 actions">
        <button class="pure-button" [disabled]="!item.isTask || item.addedToTasks" (click)="addTask()">Add Task</button>
      </div>
    </div>
  </div>
</div>
`;

let style = `
  .item {
    border-bottom: 10px solid #eee;
  }
  .title {
    font-size: 1.75em;
    margin-top: .5em;
    margin-bottom: .5em;
  }
  .description {
    font-size: 1.5em;
    margin-bottom: .5em;
  }
  .date {
    margin-bottom: 1em;
  }
  .actions {
    margin-bottom: 1em;
  }
`;

@Component({
  directives: [CORE_DIRECTIVES],
  selector: 'activity-task',
  styles: [style],
  template: template,
  encapsulation: ViewEncapsulation.Emulated
})
export class ActivityTask {
  @Input() item: Item;
  @Output() added = new EventEmitter();

  addTask() {
    this.added.next();
  }
}

import {ActivitiesService} from "../services/ActivitiesService";
import {ActivityTask} from "./activity-task";
import {Component, NgFor} from "angular2/angular2";
import {Router} from "angular2/router";
import {TasksService} from "../services/TasksService";

@Component({
  directives: [ActivityTask, NgFor],
  selector: 'activities',
  template: `
  <div *ng-for='#activity of activities'>
    <activity-task [item]="activity" (added)="addTask(activity)"></activity-task>
  </div>
  `
})
export class Activities {
  activities = [];

  constructor(public activitiesService: ActivitiesService, public tasksService: TasksService, public router: Router) {
    this.activitiesService.getActivities()
      .then(activities => this.activities = activities);
  }

  addTask(task) {
    task.addedToTasks = true;
    this.tasksService.addTask(task);
    this.router.navigate(["/Tasks"]);
  }
}

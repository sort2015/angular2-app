import "reflect-metadata";
import "zone.js";

import {ActivitiesService} from "./services/ActivitiesService";
import {TasksService} from "./services/TasksService";
import {bootstrap, Component, NgFor, provide} from "angular2/angular2";
import {HTTP_PROVIDERS} from "angular2/http";
import {
  APP_BASE_HREF,
  RouteConfig,
  Router,
  ROUTER_DIRECTIVES,
  ROUTER_PROVIDERS} from "angular2/router";
import {ROUTES_CONFIG} from "./routes";


@Component({
  directives: [ROUTER_DIRECTIVES],
  selector: "angular2-app",
  templateUrl: "app.html",
})
@RouteConfig(ROUTES_CONFIG)
class App {
  activities = [];

  constructor() {

  }

}

bootstrap(App, [
  ActivitiesService,
  TasksService,
  HTTP_PROVIDERS,
  ROUTER_PROVIDERS,
  provide(APP_BASE_HREF, {useValue: '/'})
])
.then(success => console.log(success))
.catch(error => console.log(error));

var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var faker = require('faker');

var fsfaker = function () {

  function getImages(dirname) {
    var basePath = path.join('img', dirname);
    return fs.readdirSync(path.join(__dirname, 'app', basePath)).filter(function (f) {
      return (path.extname(f) === '.jpg');
    }).map(function (item) {
      return path.join('/', basePath, item);
    });
  }

  var portraits = {
    male: getImages(path.join('portraits', 'male')),
    female: getImages(path.join('portraits', 'female'))
  };
  var memories = getImages('memories');
  var records = getImages('records');
  var dummyImages = getImages('dummy-images');

  return {
    gender: function () {
      return _.sample(['male', 'female']);
    },
    image: {
      portrait: function (gender) {
        if (gender) {
          return _.sample(portraits[gender]);
        } else {
          return _.sample(portraits.male.concat(portraits.female));
        }
      },
      memory: function () {
        return _.sample(memories);
      },
      record: function () {
        return _.sample(records);
      },
      dummy: function () {
        return _.sample(dummyImages);
      }
    }
  }
}();

var users = [
  {
    id: 1,
    username: "rob",
    password: "test",
    firstName: "Rob",
    lastName: "Hicks",
    avatar: fsfaker.image.portrait('male'),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER', 'ADMIN']
  },
  {
    id: 2,
    username: "logan",
    password: "test",
    firstName: "Logan",
    lastName: "Allred",
    avatar: fsfaker.image.portrait('male'),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER', 'ADMIN']
  },
  {
    id: 3,
    username: "user",
    password: "test",
    firstName: "Sample",
    lastName: "User",
    avatar: fsfaker.image.portrait('female'),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  },
  {
    id: 4,
    username: "admin",
    password: "test",
    firstName: "Admin",
    lastName: "User",
    avatar: fsfaker.image.portrait('female'),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER', 'ADMIN']
  },
  {
    id: 5,
    username: "bob",
    password: "test",
    firstName: "Bob",
    lastName: "Smith",
    avatar: fsfaker.image.portrait('male'),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  },
  {
    id: 6,
    username: faker.internet.userName(),
    password: "test",
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    avatar: fsfaker.image.portrait(),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  },
  {
    id: 7,
    username: faker.internet.userName(),
    password: "test",
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    avatar: fsfaker.image.portrait(),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  },
  {
    id: 8,
    username: faker.internet.userName(),
    password: "test",
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    avatar: fsfaker.image.portrait(),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  },
  {
    id: 9,
    username: faker.internet.userName(),
    password: "test",
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    avatar: fsfaker.image.portrait(),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  },
  {
    id: 10,
    username: faker.internet.userName(),
    password: "test",
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    avatar: fsfaker.image.portrait(),
    bgImage: fsfaker.image.dummy(),
    roles: ['USER']
  }
];

var people = _.times(20, function (n) {
  var person = faker.helpers.createCard();
  person.avatar = fsfaker.image.portrait();
  person.bgImage = fsfaker.image.dummy();
  return person;
});

function generateRandomActivity() {
  var user = _.sample(users);
  var person = _.sample(people);
  var num = 5 + _.random(1000);
  var activities = [
    {
      title: `Photo added to ${person.name} by @${user.username}`,
      image: fsfaker.image.memory(),
      isTask: false
    },
    {
      title: `Possible duplicates found for ${person.name}`,
      image: person.avatar,
      isTask: true
    },
    {
      title: `You submitted indexing batch #${faker.random.number()}. You have now indexed ${num} records`,
      image: fsfaker.image.record(),
      isTask: false
    },
    {
      title: `@${user.username} sent you a message`,
      image: user.avatar,
      isTask: false
    },
    {
      title: `A source was attached to ${person.name} from a record you indexed`,
      image: person.avatar,
      isTask: false
    },
    {
      title: `@${user.username} has shared an ordinance for ${person.name} with you`,
      image: person.avatar,
      isTask: true
    },
    {
      title: `You shared an ordinance for ${person.name} with @${user.username}`,
      image: person.avatar,
      isTask: false
    },
    {
      title: `@${user.username} tagged ${person.name} in a photo`,
      image: person.avatar,
      isTask: false
    },
    {
      title: `@${user.username} shared a photo with you`,
      image: fsfaker.image.memory(),
      isTask: false
    },
    {
      title: `@${user.username} attached a source to ${person.name}`,
      image: person.avatar,
      isTask: false
    },
    {
      title: `@${user.username} made changes to ${person.name}`,
      image: person.avatar,
      isTask: false
    },
    {
      title: `A relationship was added to ${person.name} by @${user.username}`,
      image: person.avatar,
      isTask: false
    },
    {
      title: `A possible record was found for ${person.name}`,
      image: fsfaker.image.record(),
      isTask: true
    },
    {
      title: `${person.name} has data inconsistencies that need to be checked`,
      image: person.avatar,
      isTask: true
    },

  ];
  return _.sample(activities);

}

module.exports = function () {
  var data = {
    people: people,
    users: users,
    messages: _.times(20, function (n) {
      return {
        from: _.sample(users).id,
        to: _.sample(users).id,
        subject: faker.lorem.sentence(),
        body: faker.lorem.sentences(_.random(7))
      }
    }),
    activity: _.sortBy(_.times(30, function (n) {
      var details = generateRandomActivity();
      return _.assign({}, details, {
        id: n,
        description: faker.lorem.sentence(),
        timestamp: _.sample([faker.date.recent(), faker.date.past(), faker.date.recent(10), faker.date.past(), faker.date.recent(5), faker.date.recent(7), faker.date.recent(14), faker.date.recent(3), faker.date.recent(7), faker.date.recent(30)]),
      });
    }), 'timestamp').reverse()
  };

  return data;
};
